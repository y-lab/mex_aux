#pragma once

#include "mex.h"

#include <mex_aux/base_types.hpp>
#include <mex_aux/kv/to_double.hpp>

#include <mex_aux/on_error.hpp>
#include <mex_aux/macro.hpp>

namespace mex_aux {
    namespace kv {
        using ::mex_aux::affine_t;

        inline mxArray *to_affari(const affine_t &v)
        {
            MEX_AUX_ERROR("notImplemented", "mex_aux::kv::to_affari is not implemented");
            MEX_AUX_UNREACHABLE();
        }
    }
}
