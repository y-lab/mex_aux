#pragma once

#include "mex.h"

#include <new>

#include <mex_aux/base_types.hpp>
#include <mex_aux/field.hpp>
#include <mex_aux/on_error.hpp>

namespace mex_aux {
    namespace kv {
        class state {
            int affine_maxnum_;

        protected:
            state()
                : affine_maxnum_(0)
            {
            }

            state(mxArray *v)
            {
                if (v == nullptr) {
                    new(this) state();
                    return;
                }

                if (!mxIsStruct(v) || !mxIsScalar(v)) {
                    ::mex_aux::on_error(
                        "mex_aux:kv:state:ctor",
                        MEX_AUX_ERROR_MSG("argument must be a scalar struct")
                    );
                }

                mxArray *p = mxGetField(v, 0, "affine_maxnum");

                if (p == nullptr) {
                    ::mex_aux::on_error(
                        "mex_aux:kv:state:ctor",
                        MEX_AUX_ERROR_MSG("argument must have a field named 'affine_maxnum'")
                    );
                }

                if (!mxIsNumeric(p)) {
                    ::mex_aux::on_error(
                        "mex_aux:kv:state:ctor",
                        MEX_AUX_ERROR_MSG("arg.affine_maxnum must be numeric")
                    );
                }

                affine_maxnum_ = static_cast<int>(mxGetScalar(p));
            }

        public:
            static state save()
            {
                state s;

                s.affine_maxnum_ = ::mex_aux::affine_t::maxnum();

                return s;
            }

            static state create()
            {
                return state();
            }

            static state create(mxArray *v)
            {
                return state(v);
            }

            void restore() const
            {
                ::mex_aux::affine_t::maxnum() = affine_maxnum_;
            }

            mxArray *to_matlab_struct() const
            {
                const char *fields[] = {"affine_maxnum"};

                mxArray *s = mxCreateStructMatrix(1, 1, 1, fields);

                ::mex_aux::set_field(s, 0, 0, mxCreateDoubleScalar(affine_maxnum_));

                return s;
            }
        };
    }
}
