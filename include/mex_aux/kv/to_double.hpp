#pragma once

#include <type_traits>

#include "mex.h"

#include <mex_aux/base_types.hpp>

namespace mex_aux {
    namespace kv {
        using ::mex_aux::vector_t;
        using ::mex_aux::matrix_t;

        inline mxArray *to_double(double v)
        {
            return mxCreateDoubleScalar(v);
        }

        template <typename E>
        inline mxArray *to_double(
            const boost::numeric::ublas::vector_expression<E> &v
        )
        {
            using value_type = typename E::value_type;

            static_assert(
                std::is_same<std::decay_t<value_type>, double>::value,
                "v must be an real vector"
            );

            auto e = v();
            auto size = e.size();

            mxArray *r = mxCreateDoubleMatrix(size, 1, mxREAL);

            for (unsigned i = 0; i < size; ++i) {
                mxGetPr(r)[i] = e(i);
            }

            return r;
        }

        template <typename E>
        inline mxArray *to_double(
            const boost::numeric::ublas::matrix_expression<E> &v
        )
        {
            using value_type = typename E::value_type;

            static_assert(
                std::is_same<std::decay_t<value_type>, double>::value,
                "v must be an real vector"
            );

            auto e = v();
            auto nrows = e.size1();
            auto ncols = e.size2();

            mxArray *r = mxCreateDoubleMatrix(nrows, ncols, mxREAL);

            for (unsigned i = 0; i < nrows; ++i) {
                for (unsigned j = 0; j < ncols; ++j) {
                    mxGetPr(r)[i + j * nrows] = e(i, j);
                }
            }

            return r;
        }
    }
}
