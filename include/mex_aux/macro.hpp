#pragma once

#if defined(_MSC_VER)
#  define MEX_AUX_UNREACHABLE() __assume(0)
#else
#  define MEX_AUX_UNREACHABLE() __builtin_unreachable()
#endif
