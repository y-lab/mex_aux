#pragma once

#include <sstream>
#include <cstring>

#include "mex.h"

#include <mex_aux/base_types.hpp>

namespace mex_aux {
    template <typename ErrorClass = exception>
    inline void on_error(
        const char *id,
        const char *func,
        const char *file,
        const char *line,
        const char *msg
    )
    {
        std::ostringstream id_ss, msg_ss;

        id_ss << "MATLAB:" << mexFunctionName();

        if (id != nullptr && std::strlen(id) != 0) {
            id_ss << ':' << id;
        }

        msg_ss << func << "(file:\"" << file << "\", line:" << line << "): " << msg;

        throw ErrorClass(id_ss.str().c_str(), msg_ss.str().c_str());
    }
}

#define MEX_AUX_ERROR_MSG_II(msg, line) __FUNCTION__, __FILE__, # line, msg
#define MEX_AUX_ERROR_MSG_I(msg, line) MEX_AUX_ERROR_MSG_II(msg, line)
#define MEX_AUX_ERROR_MSG(msg) MEX_AUX_ERROR_MSG_I(msg, __LINE__)

#define MEX_AUX_ERROR(id, msg) ::mex_aux::on_error(id, MEX_AUX_ERROR_MSG(msg))
