#pragma once

#include <stdexcept>
#include <string>
#include <utility>
#include <cstring>

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>

#include <kv/interval.hpp>
#include <kv/affine.hpp>

namespace mex_aux {
    using interval_t = kv::interval<double>;
    using affine_t = kv::affine<double>;

    template <typename T>
    using vector_t = boost::numeric::ublas::vector<T>;

    template <typename T>
    using matrix_t = boost::numeric::ublas::matrix<T>;

    class exception : public std::exception {
        char *id_;
        char *message_;

    public:
        exception(const char *id, const char *message)
            : id_(static_cast<char*>(mxMalloc(std::strlen(id))))
            , message_(static_cast<char*>(mxMalloc(std::strlen(message))))
        {
            std::strcpy(id_, id);
            std::strcpy(message_, message);
        }

        const char *what() const noexcept override
        {
            return message_;
        }

        const char *id() const
        {
            return id_;
        }
    };

    class invalid_type_error : public exception {
    public:
        using exception::exception;
    };

    class invalid_dimension_error : public exception {
    public:
        using exception::exception;
    };
}
