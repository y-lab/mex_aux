#pragma once

#include "mex.h"

#include <kv/affine.hpp>

#include <mex_aux/base_types.hpp>
#include <mex_aux/intlab/to_double.hpp>

namespace mex_aux {
    namespace kv {
        inline void init_affine()
        {
            affine_t::maxnum() = 0;
        }
    }

    namespace intlab{
        using ::mex_aux::affine_t;

        inline affine_t to_affine_scalar(
            const mxArray *v,
            bool strict = false
        )
        {
            mxArray *lhs[1], *rhs[1];
            
            return affine_t();
        }
    }
}
