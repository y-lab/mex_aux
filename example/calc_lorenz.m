% calc_lorenz.m Help file for calc_lorenz MEX-file.
%  calc_lorenz.cpp
% 
%  ローレンツ方程式の精度保証をする。
% 
%  構文:
%       result = calc_lorenz(init, t_start, t_end, n)
%       [result, status] = calc_lorenz(init, t_start, t_end, n)
%  入力:
%       init: 初期値。区間ベクトルか実ベクトル。
%       t_start: 時間の始点。区間値か実数。
%       t_end: 時間の終点。区間値か実数。
%       n: 時間の分割数。整数。
%  出力:
%       result: 結果を表す構造体。
%         resultは次のフィールドを持つ。
%
%         result.t
%           時間分点。最後まで精度保証できた場合、要素数はn+1になる。
%         result.x
%           時間分点に対応する計算結果。要素数はresult.tと同じ。
%           result.x(:, n)でn番目の結果が得られる。
%       status: 計算の成否を表す値。
%         次の3つのいずれかの値をとる。
%
%         Status.Failed 全く精度保証できなかったことを表す。
%         Status.Incomplete 途中までは精度保証できたことを表す。
%         Status.Succeeded 最後まで精度保証できたことを表す。
%

%  This is a MEX-file for MATLAB.
% 