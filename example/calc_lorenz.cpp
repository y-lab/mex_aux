#include "mex.h"

#include "mex_aux.hpp"

#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>

#include <kv/ode-maffine.hpp>
#include <kv/ode-param.hpp>

#include "lorenz.hpp"

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
    try {
        // 引数のチェック
        if (nlhs > 2) {
            MEX_AUX_ERROR("maxlhs", "Too many output arguments.");
        } else if (nrhs != 4) {
            MEX_AUX_ERROR("invalidArgument", "nrhs != 4");
        }

        // 入力引数を変換する
        vector_t<affine_t> x = mex_aux::intlab::to_interval_vector(prhs[0]);
        auto t_start = mex_aux::intlab::to_interval(prhs[1]);
        auto t_end = mex_aux::intlab::to_interval(prhs[2]);
        auto n_int = static_cast<int>(mxGetScalar(prhs[3]));
        unsigned n(n_int);

        if (x.size() != 3) {
            MEX_AUX_ERROR("invalidArgument", "length(x) != 3");
        }

        if (n_int <= 0) {
            MEX_AUX_ERROR("invalidArgument", "n must be positive");
        }

        // 計算結果はtsとxsに保存する
        vector_t<interval_t> ts(n + 1);
        matrix_t<interval_t> xs(3, n + 1);
        unsigned niter = 0;

        // 初期値を保存する
        ts(0) = t_start;
        column(xs, 0) = to_interval(x);

        lorenz func;
        int status = 2;

        while (niter < n) {
            ++niter;

            ts(niter) = t_start + (t_end - t_start) * niter / n;

            status = kv::ode_maffine(
                func, x, ts(niter - 1), ts(niter),
                kv::ode_param<double>().set_autostep(false));

            if (status == 0) {
                // 全く精度保証できなかった
                --niter;

                if (niter != 0) {
                    // niter-1回目までの反復はうまくいっている
                    status = 1;
                }

                break;
            }

            column(xs, niter) = to_interval(x);

            if (status == 1) {
                // 途中までしか精度保証できなかった
                break;
            }
        }

        // 結果を出力引数に書き込む

        const char *result_fields[] = {"t", "x"};

        // tとxをフィールドとして持つ構造体を作る
        plhs[0] = mxCreateStructMatrix(1, 1, 2, result_fields);

        // フィールドに結果を書き込む
        mex_aux::set_field(plhs[0], 0, 0, mex_aux::kv::to_intval(subrange(ts, 0, niter + 1)));
        mex_aux::set_field(plhs[0], 0, 1, mex_aux::kv::to_intval(subrange(xs, 0, 3, 0, niter + 1)));

        //　出力引数が2個以上あるときはkv::ode_maffineの戻り値も書き込む
        if (nlhs >= 2) {
            plhs[1] = mxCreateDoubleScalar(status);
        }
    } catch (mex_aux::exception &e) {
        mexErrMsgIdAndTxt(e.id(), e.what());
    } catch (...) {
        mexErrMsgIdAndTxt("MATLAB:calc_lorenz:error", "unknown error");
    }
}
