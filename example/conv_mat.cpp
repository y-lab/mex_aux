#include "mex.h"

#include <mex_aux.hpp>

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
    if (nlhs > 1) {
	    mexErrMsgIdAndTxt(
            "MATLAB:conv_mat:maxlhs",
            "Too many output arguments."
        );
    }

    auto x = mex_aux::intlab::to_interval_matrix(prhs[0]);

    plhs[0] = mex_aux::kv::to_intval(x);
}
