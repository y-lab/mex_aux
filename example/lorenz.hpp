#pragma once

#include <mex_aux.hpp>

struct lorenz {
    template <typename T>
    vector_t<T> operator()(const vector_t<T> &x, const T & t) const
    {
        vector_t<T> y(3);

        y(0) = -6.0 * x(0) + 6.0 * x(1);
        y(1) = -x(0) * x(2) + 28.0 * x(0) - x(1);
        y(2) = x(0) * x(1) - interval_t(8.0) / 3.0 * x(2);

        return y;
    }
};
