#include "mex.h"

#include <mex_aux.hpp>

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
    if (nlhs > 1) {
	    mexErrMsgIdAndTxt(
            "MATLAB:conv_vec:maxlhs",
            "Too many output arguments."
        );
    }

    // boost::numeric::ublas::vector<kv::interval<double>> x = mex_aux::intlab::to_interval_vector(prhs[0]);
    auto x = mex_aux::intlab::to_interval_vector(prhs[0]);

    plhs[0] = mex_aux::kv::to_intval(x);
}
