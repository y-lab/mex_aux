function make_example
    boost_root = request_env( ...
        'BOOST_ROOT', ...
        'Boost のインストール先を入力してください（省略可）：' ...
    );

    kv_root = request_env( ...
        'KV_PATH', ...
        'kv のインストール先を入力してください（省略可）：' ...
    );

    if ~isempty(boost_root)
        boost_root = ['-I' boost_root];
    end

    if ~isempty(kv_root)
        kv_root = ['-I', kv_root];
    end
    
    if ispc
        cxxflags = '';
    else
        cxxflags = 'CXXFLAGS="-std=c++1z"';
    end

    display('conv_vec をビルドします。');
    mex(cxxflags, boost_root, kv_root, '-I../include', 'conv_vec.cpp');

    display('conv_mat をビルドします。');
    mex(cxxflags, boost_root, kv_root, '-I../include', 'conv_mat.cpp');

    display('calc_lorenz をビルドします。');
    mex(cxxflags, boost_root, kv_root, '-I../include', 'calc_lorenz.cpp');
end

function value = request_env(name, prompt)
    value = getenv(name);

    if isempty(value)
        value = input(prompt, 's');
    end
end